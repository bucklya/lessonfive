package com.company;

import taskone.data.Generator;
import taskone.model.BookHolder;

import java.util.Scanner;

public class TaskOne {
    public void runTaskOne() {
        BookHolder bookHolder = new BookHolder(Generator.generate());
        Scanner scan = new Scanner(System.in);

        bookHolder.printOldestBook();

        System.out.println("-------------------");

        System.out.println("Введите автора которого хотите найти: ");
        String searchName = scan.nextLine();
        bookHolder.searchingBooksOfAuthor(searchName);

        System.out.println("-------------------");

        System.out.println("Введите год для поиска книг которые были изданы ранее: ");
        int earlierThan = Integer.parseInt(scan.nextLine());
        bookHolder.earlierBooks(earlierThan);

        System.out.println("-------------------");

        System.out.println("Книги с 3мя и больше авторов:");
        bookHolder.booksWithThreeOrMoreAuthors();

    }
}
