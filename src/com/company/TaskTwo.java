package com.company;

import tasktwo.data.Generator;
import tasktwo.model.BanksHolder;

import java.util.Scanner;

public class TaskTwo {
    public void runTaskTwo() {
        BanksHolder banksHolder = new BanksHolder(Generator.generate());
        Scanner scan = new Scanner(System.in);

        System.out.println("Здравствуйте! Если вы хотите купить валюту введите <<купить>>, если продать - <<продать>>. ");
        String choseBuySell = scan.nextLine();

        System.out.println("Введите сумму с которой вы хотите провести операцию: ");
        int amount = Integer.parseInt(scan.nextLine());

        System.out.println("Введите название банка с помощью кторого вы хотите провести операцию (ПриватБанк, ОщадБанк или ПУМБ): ");
        String nameBank = scan.nextLine();

        System.out.println("Введите валюту для конвертации (USD, EUR или RUB): ");
        String currency = scan.nextLine();

        banksHolder.accounting(choseBuySell, nameBank, currency, amount);
    }
}