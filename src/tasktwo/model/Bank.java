package tasktwo.model;

import java.util.Locale;

public class Bank {
    protected String name;
    protected Currency[] rates;

    public Bank(String name, Currency[] rates) {
        this.name = name;
        this.rates = rates;
    }

    public boolean isThatName(String inputNameBank) {
        return name.equalsIgnoreCase(inputNameBank);
    }

    public void buying(String currency, int amount) {
        for (Currency selectedRate : rates) {
            if (selectedRate.getNameCurrency().equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "Ваша сумма в %s при покупке будет: %.2f", currency, amount / selectedRate.getSell()));
            }
        }
    }

    static String ACTION_BUY = "купить";
    static String ACTION_SELL = "продать";
    public boolean isBuy(String choseBuySell){
        if (choseBuySell.equalsIgnoreCase(ACTION_BUY)){
            return true;
        } else {
            return false;
        }
    }

    public boolean isSell(String choseBuySell){
        if (choseBuySell.equalsIgnoreCase(ACTION_SELL)){
            return true;
        } else {
            return false;
        }
    }

    public void selling(String currency, int amount) {
        for (Currency selectedRate : rates) {
            if (selectedRate.getNameCurrency().equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "Ваша сумма в IPA при продаже %s будет: %.2f", currency, amount * selectedRate.getBuy()));
            }
        }
    }
}
