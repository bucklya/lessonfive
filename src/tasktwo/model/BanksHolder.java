package tasktwo.model;

public class BanksHolder {
    private Bank[] banks;

    public BanksHolder(Bank[] banks) {
        this.banks = banks;
    }

    public void accounting(String choseBuySell, String inputNameBank, String currency, int amount) {
        for (Bank currentBank : banks) {
            if (currentBank.isThatName(inputNameBank)) {
                if (currentBank.isBuy(choseBuySell)) {
                    currentBank.buying(currency, amount);
                } else if (currentBank.isSell(choseBuySell)){
                    currentBank.selling(currency, amount);
                }
            }
        }
    }
}