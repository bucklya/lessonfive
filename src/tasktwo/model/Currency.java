package tasktwo.model;

public class Currency {
    protected String nameCurrency;
    protected float sell;
    protected float buy;

    public Currency(String nameCurrency, float buy, float sell) {
        this.nameCurrency = nameCurrency;
        this.buy = buy;
        this.sell = sell;
    }

    public String getNameCurrency() {
        return nameCurrency;
    }
    public float getBuy() {
        return buy;
    }
    public float getSell() {
        return sell;
    }
}
