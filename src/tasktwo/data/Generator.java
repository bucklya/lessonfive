package tasktwo.data;

import tasktwo.model.Bank;
import tasktwo.model.Currency;

public final class Generator {
    private Generator() {
    }

    public static Bank[] generate() {
        Bank[] banks = new Bank[3];

        {
            Currency[] currencies = new Currency[3];
            currencies[0] = new Currency("usd", 27.4f, 28.05f);
            currencies[1] = new Currency("eur", 29.95f, 30.7f);
            currencies[2] = new Currency("rub", 0.32f, 0.36f);
            banks[0] = new Bank("ПриватБанк", currencies);
        }

        {
            Currency[] currencies = new Currency[3];
            currencies[0] = new Currency("usd", 27.2f, 28.2f);
            currencies[1] = new Currency("eur", 29.2f, 30.7f);
            currencies[2] = new Currency("rub", 0.35f, 0.38f);
            banks[1] = new Bank("ОщадБанк", currencies);
        }

        {
            Currency[] currencies = new Currency[3];
            currencies[0] = new Currency("usd", 27.25f, 28f);
            currencies[1] = new Currency("eur", 29.5f, 30.5f);
            currencies[2] = new Currency("rub", 0.33f, 0.35f);
            banks[2] = new Bank("ПУМБ", currencies);
        }

        return banks;
    }
}
