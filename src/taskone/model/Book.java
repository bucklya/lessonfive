package taskone.model;

import taskone.data.Generator;

import java.sql.SQLOutput;

public class Book {

    private String title;
    private int year;
    private String namePublishingHouses;
    private Authors[] authors;

    public Book(String title, int year, String namePublishingHouses, Authors[] authors) {
        this.title = title;
        this.year = year;
        this.namePublishingHouses = namePublishingHouses;
        this.authors = authors;
    }

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    public String getNamePublishingHouses() {
        return namePublishingHouses;
    }

    public void outputAuthors() {
        for (Authors author : authors) {
            System.out.print(author.getName() + "; ");
        }
    }

    public boolean hasAuthor(String name) {
        for (Authors author : authors) {
            if (author.getName().toLowerCase().contains(name.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public boolean hasMoreThan3Authors() {
        for (Authors author : authors) {
            if (authors.length >= 3) {
                return true;
            }
        }
        return false;
    }
}