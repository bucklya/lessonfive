package taskone.model;

public class BookHolder {

    private Book[] books;

    public BookHolder(Book[] books) {
        this.books = books;
    }

    public void printOldestBook() {
        int oldYear = books[0].getYear();
        int idBook = 0;
        for (int i = 1; i < books.length; i++) {
            if (books[i].getYear() < oldYear) {
                oldYear = books[i].getYear();
                idBook = i;
            }
        }
        System.out.print("Самая старая книга: " + books[idBook].getTitle() + " | Ее автор(ы): ");
        books[idBook].outputAuthors();
        System.out.println();
    }

    public void searchingBooksOfAuthor(String name) {
        for (Book book : books) {
            if (book.hasAuthor(name)) {
                System.out.print("Название: " + book.getTitle() + " | Издательство: " + book.getNamePublishingHouses() + " | Год издания: " + book.getYear() + " | Автор(ы): ");
                book.outputAuthors();
                System.out.println();
            }
        }
    }

    public void earlierBooks(int inputYear) {
        for (Book book : books) {
            if (book.getYear() < inputYear) {
                System.out.print("Название: " + book.getTitle() + " | Издательство: " + book.getNamePublishingHouses() + " | Год издания: " + book.getYear() + " | Автор(ы): ");
                book.outputAuthors();
                System.out.println();
            }
        }
    }

    public void booksWithThreeOrMoreAuthors(){
        for (Book book : books) {
            if(book.hasMoreThan3Authors()){
                System.out.print("Название: " + book.getTitle() + " | Издательство: " + book.getNamePublishingHouses() + " | Год издания: " + book.getYear() + " | Автор(ы): ");
                book.outputAuthors();
                System.out.println();
            }
        }
    }
}
