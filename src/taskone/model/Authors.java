package taskone.model;

public class Authors {
    private String name;

    public Authors(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
