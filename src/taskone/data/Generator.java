package taskone.data;

import taskone.model.Authors;
import taskone.model.Book;

public final class Generator {

    private Generator() {
    }

    public static Book[] generate() {
        Book[] books = new Book[7];

        {
            Authors[] authors = new Authors[2];
            authors[0] = new Authors("Илья Илаф");
            authors[1] = new Authors("Евгений Петров");
            books[0] = new Book("Двенадцать стульев", 2009, "АСТ", authors);
        }

        {
            Authors[] authors = new Authors[2];
            authors[0] = new Authors("Илья Илаф");
            authors[1] = new Authors("Евгений Петров");
            books[1] = new Book("Одноэтажная Америка", 2004, "Текст", authors);
        }

        {
            Authors[] authors = new Authors[1];
            authors[0] = new Authors("Онорио Бустос Домек");
            books[2] = new Book("Образцовое убийство", 2001, "Симпозиум", authors);
        }

        {
            Authors[] authors = new Authors[1];
            authors[0] = new Authors("Стивен Кинг");
            books[3] = new Book("Зеленая миля", 2020, "АСТ", authors);
        }

        {
            Authors[] authors = new Authors[2];
            authors[0] = new Authors("Стивен Кинг");
            authors[1] = new Authors("Питер Страуб");
            books[4] = new Book("Черный дом", 2016, "АСТ", authors);
        }

        {
            Authors[] authors = new Authors[4];
            authors[0] = new Authors("Генри Лайон Олди");
            authors[1] = new Authors("Андрей Валентинов");
            authors[2] = new Authors("Марина Дяченко");
            authors[3] = new Authors("Сергей Дяченко");
            books[5] = new Book("Рубеж", 1999, "Terra Fantastica", authors);
        }

        {
            Authors[] authors = new Authors[3];
            authors[0] = new Authors("Алексей Пехов");
            authors[1] = new Authors("Елена Бычкова");
            authors[2] = new Authors("Наталья Турчанинова");
            books[6] = new Book("Заклинатели", 2011, "Армада", authors);
        }


        return books;
    }
}
